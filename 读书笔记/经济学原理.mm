<map version="freeplane 1.9.13">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="经济学原理" FOLDED="false" ID="ID_696401721" CREATED="1610381621824" MODIFIED="1675490206120" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" associatedTemplateLocation="template:/standard-1.6.mm" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#afd3f7" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#afd3f7"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="3" RULE="ON_BRANCH_CREATION"/>
<node TEXT="经济学分类" POSITION="right" ID="ID_101462088" CREATED="1675490207305" MODIFIED="1675566685760">
<edge COLOR="#ff0000"/>
<node TEXT="微观经济学" ID="ID_799827631" CREATED="1675566686132" MODIFIED="1675566690611">
<node TEXT="研究家庭和企业如何做出决策，以及他们如何在市场上相互交易的学科" ID="ID_768682760" CREATED="1675566694994" MODIFIED="1675566740472"/>
</node>
<node TEXT="宏观经济学" ID="ID_339840310" CREATED="1675566690868" MODIFIED="1675566693998">
<node TEXT="研究整体经济现象，包括通货膨胀、失业和经济增长的学科" ID="ID_310966003" CREATED="1675566742337" MODIFIED="1675566769301"/>
</node>
</node>
<node TEXT="十大原理" POSITION="right" ID="ID_1880162987" CREATED="1675490251318" MODIFIED="1675566667482">
<edge COLOR="#0000ff"/>
<node TEXT="人如何做出决策" ID="ID_413897301" CREATED="1675490255387" MODIFIED="1675490272373">
<node TEXT="资源稀缺，导致必须做出权衡取舍的决策" ID="ID_688843351" CREATED="1675490280974" MODIFIED="1675490312449">
<node TEXT="效率，增加资源" ID="ID_794204780" CREATED="1675490387350" MODIFIED="1675490408758"/>
<node TEXT="平等，资源的分配，会降低效率" ID="ID_325679986" CREATED="1675490409087" MODIFIED="1675490499511"/>
</node>
<node TEXT="机会成本（某种东西的成本是为了得到它所放弃的东西）" ID="ID_859777847" CREATED="1675490523770" MODIFIED="1675490579424"/>
<node TEXT="理性人考虑边际量" ID="ID_854991862" CREATED="1675490721802" MODIFIED="1675490743409">
<node TEXT="假设人是理性" ID="ID_1173256266" CREATED="1675490743695" MODIFIED="1675490754912">
<node TEXT="群体心理学" ID="ID_280398909" CREATED="1675490756976" MODIFIED="1675490769942"/>
</node>
<node TEXT="理性人通常通过比较边际收益与成本来做决策" ID="ID_1935795517" CREATED="1675490807416" MODIFIED="1675490831894">
<node TEXT="边际成本" ID="ID_1368791604" CREATED="1675490772654" MODIFIED="1675490784760"/>
<node TEXT="边际收益" ID="ID_1050944176" CREATED="1675490785353" MODIFIED="1675490789708"/>
</node>
</node>
<node TEXT="人们会对激励做出反应" ID="ID_916885127" CREATED="1675490976606" MODIFIED="1675490988731">
<node TEXT="激励：惩罚或者奖励的预期" ID="ID_631906034" CREATED="1675490996760" MODIFIED="1675491013886"/>
</node>
</node>
<node TEXT="人们如何互相影响" ID="ID_1662343165" CREATED="1675491191323" MODIFIED="1675491200884">
<node TEXT="贸易可以使每个人的状况都变得更好" ID="ID_355289711" CREATED="1675491225578" MODIFIED="1675491347845">
<node TEXT="分工、熟练、效率得到提高，最后通过交换获得更的得资源" ID="ID_1775735977" CREATED="1675491304689" MODIFIED="1675491413519"/>
</node>
<node TEXT="市场通常是组织经济活动的一种好方法" ID="ID_1274630468" CREATED="1675491428232" MODIFIED="1675491447072">
<node TEXT="计划经济：中央统一决策" ID="ID_1370967274" CREATED="1675491499718" MODIFIED="1675491547631"/>
<node TEXT="市场经济：市场导向" ID="ID_1915559106" CREATED="1675491512146" MODIFIED="1675491567755">
<node TEXT="价格就是看不见的手，用来指引经济活动" ID="ID_1726420370" CREATED="1675491617739" MODIFIED="1675491651214"/>
</node>
</node>
<node TEXT="政府有时可以改善市场结果" ID="ID_398306168" CREATED="1675491732675" MODIFIED="1675491752872">
<node TEXT="只有在政府实施规则并维持对市场经济至关重要的的制度时，看不见的手才能施展其魔力" ID="ID_1941835706" CREATED="1675491768159" MODIFIED="1675491831272"/>
<node TEXT="促进效率或者促进平等" ID="ID_895406813" CREATED="1675491876182" MODIFIED="1675491897258"/>
<node TEXT="市场会失灵" ID="ID_669177690" CREATED="1675491943280" MODIFIED="1675491949241">
<node TEXT="外部性：一个人的行为对旁观者福利的影响（比如污染）" ID="ID_832964087" CREATED="1675491949560" MODIFIED="1675492014462"/>
<node TEXT="市场势力：单个人或者公司不适当地影响市场价格的能力" ID="ID_1263104038" CREATED="1675492015361" MODIFIED="1675492105715"/>
</node>
<node TEXT="市场无法保证分配的平等" ID="ID_600614946" CREATED="1675492154137" MODIFIED="1675492179058"/>
</node>
</node>
<node TEXT="整体经济如何运行" ID="ID_1224834467" CREATED="1675492223863" MODIFIED="1675492230368">
<node TEXT="一国的生活水平取决于它生产物品和服务的能力" ID="ID_77797037" CREATED="1675492231807" MODIFIED="1675492261835">
<node TEXT="生产率" ID="ID_1285943806" CREATED="1675492333280" MODIFIED="1675492336711">
<node TEXT="每一单位劳动投入所生产的物品与服务数量" ID="ID_1952344204" CREATED="1675492337963" MODIFIED="1675492375624"/>
</node>
</node>
<node TEXT="当政府发行了过多的货币时，物价上升" ID="ID_1333603741" CREATED="1675492461250" MODIFIED="1675492483466"/>
<node TEXT="社会面临通货膨胀与失业之间的短期权衡取舍" ID="ID_1315373157" CREATED="1675492550243" MODIFIED="1675492582213">
<node TEXT="长期：货币增加会导致物价上升" ID="ID_1027066646" CREATED="1675492608546" MODIFIED="1675492636489"/>
<node TEXT="短期货币增加" ID="ID_811926413" CREATED="1675492636817" MODIFIED="1675492657823">
<node TEXT="刺激社会的整体支出水平，从而增加对物品与服务的需求" ID="ID_1651505299" CREATED="1675492638982" MODIFIED="1675492696690"/>
<node TEXT="需求的增加随着时间推移，会引起企业提高物价；但同时，它也鼓励企业雇用更多的工人，并生产更多的物品与服务" ID="ID_617684144" CREATED="1675492658154" MODIFIED="1675492780240"/>
<node TEXT="雇用更多工人，则意味着更少的失业" ID="ID_957927733" CREATED="1675492781002" MODIFIED="1675492818137"/>
</node>
</node>
</node>
</node>
<node TEXT="经济问题研究" POSITION="right" ID="ID_1862274732" CREATED="1675566126949" MODIFIED="1675566672215">
<edge COLOR="#00ff00"/>
<node TEXT="观察、总结理论、再观察、改善理论" ID="ID_1658139818" CREATED="1675566136074" MODIFIED="1675566168722"/>
<node TEXT="经济问题的不可实验及再现" ID="ID_1392784713" CREATED="1675566191324" MODIFIED="1675566210523"/>
<node TEXT="简化问题，采用假设" ID="ID_478928581" CREATED="1675566262984" MODIFIED="1675566293584"/>
<node TEXT="经济模型" ID="ID_917009799" CREATED="1675566296631" MODIFIED="1675566302175">
<node TEXT="循环流量图" ID="ID_1973724692" CREATED="1675566302555" MODIFIED="1675566316119">
<node TEXT="一个说明货币如何通过市场再家庭与企业之间流动的直观经济模型" ID="ID_600279244" CREATED="1675566322153" MODIFIED="1675566353403"/>
</node>
<node TEXT="生产可能性边界" ID="ID_1580160694" CREATED="1675566433698" MODIFIED="1675566438689">
<node TEXT="表示在可得到的生产要素与生产技术既定时，一个经济所能生产的产品数量的各种组合的图形" ID="ID_1554079617" CREATED="1675566441088" MODIFIED="1675566507215"/>
<node TEXT="稀缺性、效率、权衡取舍、机会成本和经济增长" ID="ID_667447732" CREATED="1675566608534" MODIFIED="1675566635572"/>
</node>
</node>
<node TEXT="表述世界的类型" ID="ID_949783149" CREATED="1675566857132" MODIFIED="1675566960231">
<node TEXT="实证表述" ID="ID_1091139145" CREATED="1675566865557" MODIFIED="1675566966973">
<node TEXT="描述性，做出关于世界是什么样子的表述" ID="ID_1442591042" CREATED="1675566974496" MODIFIED="1675567000580"/>
<node TEXT="通过检验证据来确认或者否定实证表述" ID="ID_22504998" CREATED="1675567048146" MODIFIED="1675567089651"/>
</node>
<node TEXT="规范表述" ID="ID_273898803" CREATED="1675566874772" MODIFIED="1675566973067">
<node TEXT="规定性，做出关于世界应该是什么样子的表述" ID="ID_1910326621" CREATED="1675567001847" MODIFIED="1675567027248"/>
<node TEXT="评价规范表述则既涉及事实也涉及价值观，仅靠数据不能验证规范表述的正确与否" ID="ID_1486515306" CREATED="1675567120502" MODIFIED="1675567236548"/>
</node>
</node>
</node>
</node>
</map>
